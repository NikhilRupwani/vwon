<?php $page = 'vipclub'; include('header.php'); ?>
<main>
	<div class="banners" style="background-image: url('images/vipclub/banner.png');">
		<div class="container">
			<h1 data-aos="fade-in" data-aos-delay="100"><span  id="experience">vip</span> CLUB</h1>
		</div>
	</div>
	<div class="space vipclub">	
		<div class="container">
			<h2>VWON88 VIP Member EXCLUSIVE Rights</h2>
			<br><br>
			<center><img src="images/vipclub/vipclubtable.png" class="img-responsive"></center>
			<br><br>
			<ul>
				<li>Above VIP membership is based on overall Total Deposit of each player's profile which registered at website VWON88.</li>
				<li>Membership entitlement brings you lifetime exclusive benefits, treatments and rewards.</li>
				<li>Above entitlements are open to all VIP members of VWON88.</li>
				<li>Poker, Lottery and any both sides betting found are not taken into calculation of total betting.</li>
				<li>In any event found that there is any usage of multiple accounts, all free credits will be confiscated and account will be suspended.</li>
				<li>VWON88 has the right to amend or terminate this promotion without any prior notice.</li>
			</ul>
		</div>
	</div>
</main>
<?php include('footer.php'); ?>