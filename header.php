<!DOCTYPE html>
<html lang="en">
<head>
  <title>VWON</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/scrollAnimation/css/styles.css" />
  <link rel="stylesheet" href="css/scrollAnimation/dist/aos.css" />
  <style type="text/css">
  	@font-face {
		font-family: "Blacksword";
		src: url("./css/fonts/Blacksword.otf");
	}
  	@font-face {
		font-family: "Itca";
		src: url("./css/fonts/BebasKai.ttf");
	}
	h1 {
		font-family: 'Itca';
	}
	#experience{
		font-family: 'Blacksword';
	}
  </style>
</head>
<body>
<header>
	<div class="container">
		<div class="desktopnav">
			<div class="topHeader">
				<div class="row">
					<div class="col-sm-6 col1">
						<a href="index.php"><img src="images/header/logo.png" class="img-responsive"></a>
					</div>
					<div class="col-sm-6 col2" style="float: right;">
						<ul>
							<li><img src="images/header/flag1.png" class="img-responsive"></li>
							<li><img src="images/header/flag2.png" class="img-responsive"></li>
							<li><img src="images/header/flag3.png" class="img-responsive"></li>
							<li><p style="margin-top: -10px;">Forgot login details? </p></li>
							<li><a href="login.php" style="color: white;"><button class="login"> LOGIN </button></a></li>
							<li><a href="login.php" style="color: white;"><button class="register"> JOIN NOW </button></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="mainHeader">
				<nav class="navbar navbar-inverse">
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span> 
				      </button>
				    </div>
				    <div class="collapse navbar-collapse" id="myNavbar">
				      <ul class="nav navbar-nav">
				        <li class="<?php if($page=='sport'){echo 'active';}?>"><a href="sports.php" style="padding-left: 0px;"><img src="images/header/icon 9.png" alt="icon1"><span> Sport</span></a></li>
				        <li class="<?php if($page=='livescore'){echo 'active';}?>"><a href="livescore.php"><img src="images/header/icon 8.png" alt="icon1"><span> Livescore</span></a></li>
				        <li class="<?php if($page=='casino'){echo 'active';}?>"><a href="casino.php"><img src="images/header/icon 7.png" alt="icon1"><span> Casino</span></a></li> 
				        <li class="<?php if($page=='slots'){echo 'active';}?>"><a href="slots.php"><img src="images/header/icon 6.png" alt="icon1"><span> Slots</span></a></li> 
				        <li class="<?php if($page=='lottery'){echo 'active';}?>"><a href="lottery.php"><img src="images/header/icon 5.png" alt="icon1"><span> Lottery</span></a></li>
				        <li class="<?php if($page=='cockfight'){echo 'active';}?>"><a href="cockfight.php"><img src="images/header/icon 4.png" alt="icon1"><span> Cockfight</span></a></li>
				        <li class="<?php if($page=='promotion'){echo 'active';}?>"><a href="promotion.php"><img src="images/header/icon 3.png" alt="icon1"><span> Promotions</span></a></li>
				        <li class="<?php if($page=='vipclub'){echo 'active';}?>"><a href="vipclub.php"><img src="images/header/icon 2.png" alt="icon1"><span> VIP club</span></a></li>
				        <li class="<?php if($page=='contact'){echo 'active';}?>"><a href="contact.php" style="padding-right: 0px;"><img src="images/header/icon 1.png" alt="icon1"><span> Contact us</span></a></li>
				      </ul>
				    </div>
				</nav>
			</div>
		</div>
		<div class="mobilenav">
			<nav class="navbar navbar-inverse">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar1">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span> 
			      </button>
			      <a href="index.php"><img src="images/header/logo.png" class="img-responsive"></a>
			    </div>
			    <div class="collapse navbar-collapse" id="myNavbar1">
			      <ul class="nav navbar-nav">
			        	<li class="<?php if($page=='sport'){echo 'active';}?>"><a href="sports.php"><img src="images/header/icon 9.png" alt="icon1"><span> Sport</span></a></li>
				        <li class="<?php if($page=='livescore'){echo 'active';}?>"><a href="livescore.php"><img src="images/header/icon 8.png" alt="icon1"><span> Livescore</span></a></li>
				        <li class="<?php if($page=='casino'){echo 'active';}?>"><a href="casino.php"><img src="images/header/icon 7.png" alt="icon1"><span> Casino</span></a></li> 
				        <li class="<?php if($page=='slots'){echo 'active';}?>"><a href="slots.php"><img src="images/header/icon 6.png" alt="icon1"><span> Slots</span></a></li> 
				        <li class="<?php if($page=='lottery'){echo 'active';}?>"><a href="lottery.php"><img src="images/header/icon 5.png" alt="icon1"><span> Lottery</span></a></li>
				        <li class="<?php if($page=='cockfight'){echo 'active';}?>"><a href="cockfight.php"><img src="images/header/icon 4.png" alt="icon1"><span> Cockfight</span></a></li>
				        <li class="<?php if($page=='promotion'){echo 'active';}?>"><a href="promotion.php"><img src="images/header/icon 3.png" alt="icon1"><span> Promotions</span></a></li>
				        <li class="<?php if($page=='vipclub'){echo 'active';}?>"><a href="vipclub.php"><img src="images/header/icon 2.png" alt="icon1"><span> VIP club</span></a></li>
				        <li class="<?php if($page=='contact'){echo 'active';}?>"><a href="contact.php" style="padding-right: 0px;"><img src="images/header/icon 1.png" alt="icon1"><span> Contact us</span></a></li>
			      </ul>
			    </div>
			  </div>
			</nav>
			<div class="topHeader">
				<ul style="float: left;">
					<li><p style="margin-top: -10px;">Forgot login details? </p></li>
					<li><a href="login.php" style="color: white;"><button class="login"> LOGIN </button></a></li>
					<li><a href="login.php" style="color: white;"><button class="register"> JOIN NOW </button></a></li>
				</ul>
			</div>
		</div>
	</div>
</header>