<?php $page = 'lottery'; include('header.php'); ?>
<main>
	<div class="banners" style="background-image: url('images/lottery/banner1.png');">
		<div class="container">
			<h1 data-aos="fade-in" data-aos-delay="100"><span  id="experience">lottery</span></h1>
		</div>
	</div>
	<div class="space lottery">
		<div style="background-color: black;">
			<div class="container">
				<ul class="nav nav-pills">
				    <li class="active"><a data-toggle="pill" href="#all">All</a></li>
				    <li><a data-toggle="pill" href="#menu1">Featured</a></li>
				    <li><a data-toggle="pill" href="#menu2">Top Rated</a></li>
				    <li><a data-toggle="pill" href="#menu3">New</a></li>
				</ul>
			</div>
		</div>  
		<div class="container"> 
			<div class="tab-content">
			    <div id="all" class="tab-pane fade in active">
			    	<h1>Try your fortune and win lottery</h1>
			    	<br><br>
			    	<div class="lotterytable">
			    		<img src="images/lottery/lotterytable.png" class="img-responsive">
			    	</div>
			    	<div class="row">
			    		<div class="col-sm-4">
			    			<img src="images/lottery/Layer 89.png" class="img-responsive">
			    		</div>
			    		<div class="col-sm-4">
			    			<img src="images/lottery/Layer 90.png" class="img-responsive">
			    		</div>
			    		<div class="col-sm-4">
			    			<img src="images/lottery/Layer 90.png" class="img-responsive">
			    		</div>
			    		<div class="col-sm-4">
			    			<img src="images/lottery/Layer 92.png" class="img-responsive">
			    		</div>
			    		<div class="col-sm-4">
			    			<img src="images/lottery/Layer 93.png" class="img-responsive">
			    		</div>
			    		<div class="col-sm-4">
			    			<img src="images/lottery/Layer 94.png" class="img-responsive">
			    		</div>
			    	</div>

			    	
			    </div>
			    <div id="menu1" class="tab-pane fade">
			    </div>
			    <div id="menu2" class="tab-pane fade">
			    </div>
			    <div id="menu3" class="tab-pane fade">
			    </div>
			</div>
		</div>
	</div>
</main>
<?php include('footer.php'); ?>