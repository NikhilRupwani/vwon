<?php $page = 'sport'; include('header.php'); ?>
<main>
	<div class="banners" style="background-image: url('images/sports/banner.png');">
		<div class="container">
			<h1 data-aos="fade-in" data-aos-delay="100"><span  id="experience">sports</span>BOOK</h1>
		</div>
	</div>
	<div class="space">
		<div style="background-color: black;">
			<div class="container">
				<ul class="nav nav-pills">
				    <li class="active"><a data-toggle="pill" href="#all">All</a></li>
				    <li><a data-toggle="pill" href="#menu1">Featured</a></li>
				    <li><a data-toggle="pill" href="#menu2">Top Rated</a></li>
				    <li><a data-toggle="pill" href="#menu3">New</a></li>
				</ul>
			</div>
		</div>  
		<div class="container">
			<div class="tab-content">
			    <div id="all" class="tab-pane fade in active">
			    	<h2>Watch all live score here</h2>
			    	<br><br>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/sports/img1.png" class="img-responsive">
				    			<h4>M-SPORTS</h4>
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/sports/img2.png" class="img-responsive">
				    			<h4>S-SPORTS</h4>
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
			    		</div>

			    	</div>
			    </div>
			    <div id="menu1" class="tab-pane fade">
			    	<h2>Watch all live score here</h2>
			    	<br><br>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/sports/img2.png" class="img-responsive">
				    			<h4>998BET</h4>
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/sports/img1.png" class="img-responsive">
				    			<h4>998BET</h4>
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    	</div>
			    </div>
			    <div id="menu2" class="tab-pane fade">
			    	<h2>Watch all live score here</h2>
			    	<br><br>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/sports/img1.png" class="img-responsive">
				    			<h4>998BET</h4>
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
			    		</div>
			    	</div>
			    </div>
			    <div id="menu3" class="tab-pane fade">
			    	<h2>Watch all live score here</h2>
			    	<br><br>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/sports/img2.png" class="img-responsive">
				    			<h4>998BET</h4>
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    	</div>
			    </div>
			</div>
		</div>
	</div>
</main>
<?php include('footer.php'); ?>