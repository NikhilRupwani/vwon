<?php $page = 'contact'; include('header.php'); ?>
<main>
	<div class="container contact">
		<h1 data-aos="fade-in" data-aos-delay="100"><span  id="experience">contact</span>  US</h1>
		<div class="box1"></div>
		<div class="box2">
			<div class="row">
				<div class="col-sm-4">
					<br>
					<div class="row">
						<div class="col-sm-6">
							<img src="images/contact/scan1.png" class="img-responsive">
						</div>
						<div class="col-sm-6">
							<img src="images/contact/scan2.png" class="img-responsive">
						</div>
					</div>
					<h4>Webchat :  v88-cs1 / v88-cs2</h4>
					<h4>Line :  v88-cs1 / v88-cs2</h4>
					<h4>Send :  v88-cs1 / v88-cs2</h4>
				</div>
				<div class="col-sm-8">
					<h2 style="text-transform: unset;">Enquiry Form</h2>
					<form action="#">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							    <input type="text" class="form-control" id="fullname" placeholder="Full Name" required>
							</div>
							<div class="form-group">
							    <input type="text" class="form-control" id="contactno" placeholder="Contact Number" required>
							</div>
							<div class="form-group">
							    <input type="email" class="form-control" id="email" placeholder="Email" required>
							</div>
							<div class="form-group">
							    <input type="text" class="form-control" id="company" placeholder="Company" required>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							    <input type="text" class="form-control" id="fullname" placeholder="Full Name" required>
							</div>
							<div class="form-group">
							    <input type="text" class="form-control" id="contactno" placeholder="Contact Number" required>
							</div>
							<div class="form-group">
							    <input type="email" class="form-control" id="email" placeholder="Email" required>
							</div>
							<div class="form-group">
							    <input type="text" class="form-control" id="company" placeholder="Company" required>
							</div>
						</div>
					</div>
					<button type="submit" class="btn playnow">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include('footer.php'); ?>	