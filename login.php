<?php $page = 'login'; include('header.php'); ?>
<main>
	<div class="login container">
		<div class="row">
			<div class="col-sm-4">
				<div class="col4">
					<h1><img src="images/login/login.png"> LOGIN</h1>
					<form action="#">
					  <div class="form-group">
					    <input type="text" class="form-control" id="username" placeholder="User Name">
					  </div>
					  <div class="form-group">
					    <input type="password" class="form-control" id="pwd" placeholder="Password">
					  </div>
					  <br>
					  <button type="submit" class="btn playnow">PLAY NOW</button>
					  <div class="form-group">
					    <label><a href="#">Forgot Password?</a></label>
					  </div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="col8">
					<h2><img src="images/login/join.png"> JOIN NOW</h2>
					<form action="#">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							    <input type="text" class="form-control" id="jusername" placeholder="User Name" required>
							</div>
							<div class="form-group">
							    <input type="email" class="form-control" id="email" placeholder="Email" required>
							</div>
							<div class="form-group">
							    <input type="password" class="form-control" id="cpwd" placeholder="Confirm Password" required>
							</div>
							<div class="form-group">
							    <input type="text" class="form-control" id="referer" placeholder="Referer" required>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							    <input type="text" class="form-control" id="fullname" placeholder="Full Name" required>
							</div>
							<div class="form-group">
							    <input type="password" class="form-control" id="jpwd" placeholder="Password" required> 
							</div>
							<div class="form-group">
							    <input type="text" class="form-control" id="mobileno" placeholder="Mobile Number" required>
							</div>
							<button type="submit" class="btn playnow">PLAY NOW</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include('footer.php'); ?>