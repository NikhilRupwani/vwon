<?php $page = 'promotion'; include('header.php'); ?>
<main>
	<div class="banners" style="background-image: url('images/promotion/banner 1.png');">
		<div class="container">
			<h1 data-aos="fade-in" data-aos-delay="100"><span  id="experience">promotion</span></h1>
		</div>
	</div>
	<div class="space promotion">
		<div style="background-color: black;">
			<div class="container">
				<ul class="nav nav-pills">
				    <li class="active"><a data-toggle="pill" href="#all">All</a></li>
				    <li><a data-toggle="pill" href="#menu1">Featured</a></li>
				    <li><a data-toggle="pill" href="#menu2">Top Rated</a></li>
				    <li><a data-toggle="pill" href="#menu3">New</a></li>
				</ul>
			</div>
		</div>  
		<div class="container">
			<div class="tab-content">
			    <div id="all" class="tab-pane fade in active">
			    	<h2>Promotion Games are Here</h2>
			    	<br><br>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 89.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 90.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 92.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 94.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
				    </div>
				    <div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 95.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 96.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 97.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 98.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
			    		</div>
			    	</div>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 99.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 100.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 102.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 103.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    	</div>
			    	<div class="row">	
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 104.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 105.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    	</div>
			    </div>
			    <div id="menu1" class="tab-pane fade">
			    	<h2>Featured Promotion Game</h2>
			    	<br><br>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 95.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 96.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 97.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    	</div>
			    </div>
			    <div id="menu2" class="tab-pane fade">
			    	<h2>Top Rated Promotion Game</h2>
			    	<br><br>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 98.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 99.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>	
				    	</div>
			    	</div>
			    </div>
			    <div id="menu3" class="tab-pane fade">
			    	<h2>New Live Casino Game</h2>
			    	<br><br>
			    	<div class="row">
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 100.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 102.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 103.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 104.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    		<div class="col-sm-3">
			    			<center>
			    				<img src="images/promotion/Layer 105.png" class="img-responsive">
				    			<button class="btn playnow">PLAY NOW</button>
				    		</center>		
			    		</div>
			    	</div>
			    </div>
			</div>
		</div>
	</div>
</main>
<?php include('footer.php'); ?>